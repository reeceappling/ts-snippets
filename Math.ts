var maxPrimes: number[] = [2,3];

function firstNPrimes(n: number) : number[]{
    let primes: number[] = maxPrimes;
    let sixes: number = maxPrimes.length>2 ? Math.round(maxPrimes[maxPrimes.length-1]/6)+1 : 1; 
    while(primes.length<n){
	    let newPrimes: number[] = primesNear(6*sixes,primes);
	    for(var i=0;i<newPrimes.length;i++){
		    primes.push(newPrimes[i]);
	    }
	    sixes++;
    }
    return primes.slice(0,n+1);
}

function primesNear(toCheck: number,primes: number[]) : number[]{
	let newPrimes: number[] = [];
	if(isPrime(toCheck-1,primes)&&!primes.includes(toCheck-1)){newPrimes.push(toCheck-1);}
	if(isPrime(toCheck+1,primes)&&!primes.includes(toCheck+1)){newPrimes.push(toCheck+1);}
	return newPrimes;
}

function isPrime(toCheck: number, primes: number[]) : boolean{
    if(primes.includes(toCheck)){return true;}
	if(toCheck<1){return false;}
	for(var i=0;i<primes.length;i++){
		if(toCheck % primes[i]==0&&toCheck!== primes[i]){
            return false;
        }
	}
	return true;
}

function sumFirstNPrimes(n: number) : number{
    var sum: number = 0;
    let primes: number[] = (maxPrimes.length>n ? maxPrimes : firstNPrimes(n));
    for(var i=0;i<n;i++){sum = sum+primes[i];}
    return sum;
}

function nthPrime(n: number) : number{
    if(maxPrimes.length>n){return maxPrimes[n]}
    let primes = firstNPrimes(n);
    return primes[primes.length-1];
}

export {
	isPrime,
	sumFirstNPrimes,
	firstNPrimes,
	nthPrime,
	primesNear
};