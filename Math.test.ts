import * as testMath from "./Math";
//const source1 = require('./source1.js');

describe("tests",()=>{

    test("nth prime with generation of primes",()=>{
        expect(testMath.nthPrime(6)).toBe(13);
    });
    
    test("Sum of first 1000 primes is 3682913 (sum generative case)",()=>{
        expect(testMath.sumFirstNPrimes(1000)).toBe(3682913);
    });

    test("Sum of first 3 primes is 10 (sum shortcut case)",()=>{
        expect(testMath.sumFirstNPrimes(3)).toBe(10);
    });

    test("The 810th prime number is 6229",()=>{
        expect(testMath.nthPrime(810)).toBe(6229);
    });

    test("Primes adjacent to 12 are 11 and 13",()=>{
        expect(testMath.primesNear(12,[2,3,5,7]).includes(11)&&testMath.primesNear(12,[2,3,5,7]).includes(13)).toBe(true);
    });

    test("5281 is Prime results in true",()=>{
        expect(testMath.isPrime(5281,testMath.firstNPrimes(1000))).toBe(true);
    });

    test("Negative numbers are not prime",()=>{
        expect(testMath.isPrime(-17,testMath.firstNPrimes(10))).toBe(false);
    });
});