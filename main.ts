import * as Math from "./Math";

export { 
    Math /* Mathematics, Prime numbers */
};

//Test with: npx jest
//Check coverage with: npx jest --coverage